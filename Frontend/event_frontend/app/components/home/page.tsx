"use client";
import React, { useEffect, useState, useCallback, useRef } from "react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import Cookies from "js-cookie";
import { useDispatch, useSelector } from "react-redux";
import { setLocation } from "../../../redux/slice/locationslice"; // Import your action
import { RootState } from "../../../redux/store"; // Import RootState type
import {
  showUnder10Km,
  hideUnder10Km,
} from "../../../redux/slice/visibleslice";

type Location = {
  latitude: number | null;
  longitude: number | null;
  cityId: number | null;
};

type Result = {
  id: number;
  event_name: string;
  event_image: string;
};

type City = {
  id: number;
  name: string;
  icon: string;
  latitude: number;
  longitude: number;
  cityId: number | null;
};

const HomePage: React.FC = () => {
  const dispatch = useDispatch();
  const location = useSelector((state: RootState) => state.location); // Get location from Redux store
  const [locationError, setLocationError] = useState<string | null>(null);
  const [city, setCity] = useState<string | null>("Mumbai");
  const [backgroundColor, setBackgroundColor] = useState<string>("transparent");
  const [query, setQuery] = useState("");
  const [results, setResults] = useState<Result[]>([]);
  const [fetchError, setFetchError] = useState<string | null>(null);
  const [searchInitiated, setSearchInitiated] = useState(false);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isLocationModalOpen, setIsLocationModalOpen] = useState(false);
  const [cities, setCities] = useState<City[]>([]);
  const [cookieLocation, setCookieLocation] = useState<Location | null>(null);
  const [cityId, setCityId] = useState(2);
  const searchBoxRef = useRef<HTMLDivElement>(null); // Ref for search result box

  const router = useRouter();

  useEffect(() => {
    const handleScroll = () => {
      const scrollY = window.scrollY;
      if (scrollY > 0) {
        setBackgroundColor("rgba(0, 0, 0, 0.9)");
      } else {
        setBackgroundColor("transparent");
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    const fetchCities = async () => {
      try {
        const response = await fetch("http://localhost:3001/cities");
        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }
        const data: City[] = await response.json();
        setCities(data);
      } catch (error) {
        console.error("Error fetching cities:", error);
      }
    };

    fetchCities();
  }, []);

  useEffect(() => {
    const userLocation = Cookies.get("UserLocation");

    if (userLocation) {
      const { id, name, latitude, longitude } = JSON.parse(userLocation);
      dispatch(setLocation({ latitude, longitude, cityId: id }));
      setCity(name);
    } else {
      // Default handling if no city is found in cookies
      const defaultCity = cities.find((city) => city.id === cityId);
      if (defaultCity) {
        const { latitude, longitude } = defaultCity;
        dispatch(setLocation({ latitude, longitude, cityId: defaultCity.id }));
        setCity(defaultCity.name);
        Cookies.set(
          "UserLocation",
          JSON.stringify({
            id: defaultCity.id,
            name: defaultCity.name,
            latitude,
            longitude,
          })
        );
      }
    }
  }, [cities, cityId, dispatch]);

  const handleUseCurrentLocation = () => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        async (position) => {
          const { latitude, longitude } = position.coords;
          // Dispatch with latitude and longitude only
          dispatch(setLocation({ cityId: null, latitude, longitude }));
          setLocationError(null);
          Cookies.set("UserLocation", JSON.stringify({ latitude, longitude }));

          try {
            // Fetch the city name based on the current location
            const cityName = await fetchCityName(latitude, longitude);
            setCity(cityName);
            setIsLocationModalOpen(false);
            dispatch(showUnder10Km()); // Show events under 10km
          } catch (error) {
            console.error("Error fetching city name:", error);
          }
        },
        (error) => {
          setLocationError(error.message);
        }
      );
    } else {
      setLocationError("Geolocation is not supported by this browser.");
    }
  };

  useEffect(() => {
    const token = Cookies.get("AccessToken");
    // console.log("token", token);
    if (token) {
      setIsLoggedIn(true);
    }
  }, [router]);

  const debounce = (func: (...args: any[]) => void, wait: number) => {
    let timeout: NodeJS.Timeout;
    return (...args: any[]) => {
      clearTimeout(timeout);
      timeout = setTimeout(() => func(...args), wait);
    };
  };

  const handleClick = (id: number) => {
    router.push(`/details/${id}`);
  };

  const fetchSearchResults = async (query: string) => {
    if (!query) {
      setResults([]);
      setSearchInitiated(false);
      return;
    }

    setSearchInitiated(true);

    const apiUrl = `http://localhost:3001/allevents?search=${query}`;

    try {
      const response = await fetch(apiUrl);
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const data = await response.json();
      console.log("Fetched data:", data);
      setResults(data.events);
      setFetchError(null);
    } catch (error) {
      console.error("Error fetching search results:", error);
      setFetchError("Error fetching search results");
      setResults([]);
    }
  };

  const debouncedFetchSearchResults = useCallback(
    debounce((query) => fetchSearchResults(query), 300),
    []
  );

  useEffect(() => {
    debouncedFetchSearchResults(query);
  }, [query]);

  const fetchCityName = async (lat: number, lon: number): Promise<string> => {
    try {
      const response = await fetch(
        `https://nominatim.openstreetmap.org/reverse?format=json&lat=${lat}&lon=${lon}`
      );

      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }

      const data = await response.json();
      const city =
        data.address.city || data.address.town || data.address.village;

      // Update cookies with the new location
      Cookies.set(
        "UserLocation",
        JSON.stringify({ latitude: lat, longitude: lon, name: city })
      );

      // Send location to backend
      sendLocationToBackend(lat, lon);

      return city;
    } catch (error) {
      console.error("Error fetching city:", error);
      return "Unknown";
    }
  };

  const sendLocationToBackend = async (latitude: number, longitude: number) => {
    const apiUrl = `http://localhost:3001/updateLocation`;

    try {
      const response = await fetch(apiUrl, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ latitude, longitude }),
      });

      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }

      console.log("Location sent to backend successfully");
    } catch (error) {
      console.error("Error sending location to backend:", error);
    }
  };

  const handleLogout = () => {
    Cookies.remove("AccessToken");
    setIsLoggedIn(false);
    router.push("/");
  };

  const clearSearch = () => {
    setQuery("");
    setResults([]);
    setSearchInitiated(false);
  };

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (
        searchBoxRef.current &&
        !searchBoxRef.current.contains(event.target as Node)
      ) {
        clearSearch();
      }
    };

    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);
  const handleCitySelect = (selectedCity: City) => {
    const { id, name, latitude, longitude } = selectedCity;

    setCity(name);
    setIsLocationModalOpen(false);

    // Save city information in cookies
    Cookies.set(
      "UserLocation",
      JSON.stringify({ id, name, latitude, longitude })
    );
    dispatch(setLocation({ latitude, longitude, cityId: id }));
    dispatch(hideUnder10Km());
  };

  useEffect(() => {
    const userLocationCookie = Cookies.get("UserLocation");
    if (userLocationCookie) {
      const { latitude, longitude } = JSON.parse(userLocationCookie);
      if (latitude && longitude) {
        dispatch(setLocation({ latitude, longitude }));
      }
    }
  }, [dispatch]);

  useEffect(() => {
    if (cookieLocation && cookieLocation.latitude && cookieLocation.longitude) {
      dispatch(
        setLocation({
          latitude: cookieLocation.latitude,
          longitude: cookieLocation.longitude,
        })
      ); // Dispatch to Redux
      sendLocationToBackend(cookieLocation.latitude, cookieLocation.longitude);
    }
  }, [cookieLocation, dispatch]);

  return (
    <div>
      <div
        className="font-sans fixed w-full top-0 z-50"
        style={{ backgroundColor }}
      >
        <header className="flex justify-between items-center py-4 md:px-8 container mx-auto">
          <div className="flex items-center space-x-8">
            <div className="flex items-center space-x-0">
              <img
                src={"/images/logo1.gif"}
                alt="Logo"
                className="h-12 w-12 mr-2"
              />
              <div className="text-sm md:text-3xl font-bold text-white px-2">
                <Link href="/">eazyEvents</Link>
              </div>
            </div>

            <div className="text-sm text-white">
              {location.latitude !== null && location.longitude !== null ? (
                <div className="relative">
                  <button
                    className="flex items-center bg-transparent border-2 border-white text-white p-1 md:p-2 rounded-lg"
                    onClick={() => setIsLocationModalOpen(true)}
                  >
                    <span>{city || "Select City"}</span>
                  </button>
                  {isLocationModalOpen && (
                    <div
                      className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50"
                      onClick={() => setIsLocationModalOpen(false)}
                    >
                      <div
                        className="bg-white p-10 rounded-lg shadow-lg max-w-3xl w-full max-h-full overflow-y-auto"
                        onClick={(e) => e.stopPropagation()}
                      >
                        <div className="flex justify-center mb-4">
                          <button
                            className="text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
                            onClick={handleUseCurrentLocation}
                          >
                            Use my current location
                          </button>
                        </div>
                        <div className="flex flex-wrap justify-center items-center mb-4 cursor-pointer">
                          {cities.map((city) => (
                            <div
                              key={city.id}
                              className="flex flex-col items-center text-center w-1/5 px-2"
                              onClick={() => handleCitySelect(city)}
                            >
                              <img
                                src={city.icon}
                                alt={`${city.name} Icon`}
                                className="h-20 w-20"
                              />
                              <span className="text-black font-bold">
                                {city.name}
                              </span>
                            </div>
                          ))}
                        </div>
                        <h2 className="text-lg font-bold">Your Location</h2>
                        <p>
                          Latitude: {location.latitude}
                          <br />
                          Longitude: {location.longitude}
                        </p>
                        <div className="mt-4 flex justify-end">
                          <button
                            className="px-4 py-2 bg-blue-500 text-white rounded-lg"
                            onClick={() => setIsLocationModalOpen(false)}
                          >
                            Close
                          </button>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              ) : (
                <div className="bg-transparent border-2 border-white text-white p-2 rounded-lg">
                  {locationError || "Fetching location..."}
                </div>
              )}
            </div>

            <form
              className="w-26 md:w-96 relative"
              onSubmit={(e) => e.preventDefault()}
            >
              <input
                type="text"
                value={query}
                onChange={(e) => setQuery(e.target.value)}
                placeholder="Search..."
                className="w-full px-3 py-1 rounded-lg bg-gray-200 text-gray-800 focus:outline-none focus:bg-white focus:ring-2 focus:ring-gray-300"
              />
              {searchInitiated && (
                <button
                  type="button"
                  className="absolute right-0 top-0 h-full px-3 py-1 text-gray-500 focus:outline-none"
                  onClick={clearSearch}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={2}
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M6 18L18 6M6 6l12 12"
                    />
                  </svg>
                </button>
              )}
            </form>
          </div>
          <div className="block md:hidden relative">
            <button
              className="text-white"
              onClick={() => setIsMenuOpen(!isMenuOpen)}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                strokeWidth={2}
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M4 6h16M4 12h16M4 18h16"
                />
              </svg>
            </button>
            {isMenuOpen && (
              <div className="absolute right-0 mt-2 w-48 bg-white shadow-lg rounded-lg">
                <div className="text-sm text-gray-800 px-3 py-1 font-bold border-b">
                  <Link href="/">Home</Link>
                </div>

                {isLoggedIn ? (
                  <>
                    <div className="text-sm text-gray-800 px-3 py-1">
                      <Link href={`/booking`}>Bookings</Link>
                    </div>
                    <div className="text-sm text-black px-3 py-1">
                      <button onClick={handleLogout}>Logout</button>
                    </div>
                  </>
                ) : (
                  <>
                    <div className="text-sm text-black px-3 py-1">
                      <Link href={`/register/login`}>Login</Link>
                    </div>
                    <div className="text-sm  text-black px-3 py-1 font-bold">
                      <Link href={`/register/signin`}>Sign In</Link>
                    </div>
                  </>
                )}
              </div>
            )}
          </div>

          <div className="hidden md:flex justify-between items-center">
            <div className="text-sm text-white px-3 py-1">
              <Link href="/">Home</Link>
            </div>

            {isLoggedIn ? (
              <>
                <div className="text-sm text-white px-3 py-1 ">
                  <Link href={`/booking`}>Bookings</Link>
                </div>
                <div className="text-sm text-white px-3 py-1">
                  <button onClick={handleLogout}>Logout</button>
                </div>
              </>
            ) : (
              <>
                <div className="text-sm text-white px-3 py-1">
                  <Link href={`/register/login`}>Login</Link>
                </div>
                <div className="text-sm text-white px-3 py-1">
                  <Link href={`/register/signin`}>Sign In</Link>
                </div>
              </>
            )}
          </div>
        </header>
      </div>

      {searchInitiated && (
        <main
          ref={searchBoxRef}
          className="fixed z-10 top-20 left-1/2 transform -translate-x-1/2 p-4 bg-white md:w-96 w-80 max-h-96 overflow-y-scroll"
        >
          <h1 className="md:text-2xl text-lg font-bold">Search Results</h1>
          {fetchError && <p className="text-red-500">{fetchError}</p>}
          <ul className="mt-4">
            {results.length > 0 ? (
              results.map((result) => (
                <li
                  key={result.id}
                  className="p-2 border-b border-gray-200 flex justify-between cursor-pointer"
                  onClick={() => handleClick(result.id)}
                >
                  <img className="w-20 p-1" src={result.event_image} />
                  {result.event_name}
                </li>
              ))
            ) : (
              <li className="text-red">No results found</li>
            )}
          </ul>
        </main>
      )}
    </div>
  );
};
export default HomePage;
