"use client";
import React from "react";
import { useRouter } from "next/navigation";
import Link from "next/link";

const Today = () => {
  const router = useRouter();
  return (
    <div className="container mx-auto">
      <div className="mt-10 flex justify-left space-x-10">
        <Link href={"/todaysevents"}>
          <img
            src="/images/TodayF.png"
            alt="Today's Events"
            className="h-50 w-50 cursor-pointer"
          />
        </Link>
        <Link href={"/weeklyevents"}>
          <img
            src="/images/weekly.png"
            alt="Weekly Events"
            className="h-50 w-50 cursor-pointer"
          />
        </Link>
      </div>
    </div>
  );
};

export default Today;
