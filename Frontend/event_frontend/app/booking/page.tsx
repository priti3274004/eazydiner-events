"use client";
import React, { useState, useEffect } from "react";
import Cookies from "js-cookie";
import { format, isBefore, startOfToday } from "date-fns";
import { jwtDecode } from "jwt-decode";
import Link from "next/link";
import Swal from "sweetalert2";
import { useRouter } from "next/navigation";

interface Booking {
  id: number;
  booking_date: string;
  no_of_persons: number;
  status: string;
  ticket: {
    ticket_name: string;
    cost: number;
    ticket_date: string;
    session: {
      session: string;
      start_time: string;
      end_time: string;
      events: {
        id: number;
        event_name: string;
        event_image: string;
      };
    };
  };
}

interface DecodedToken {
  id: number;
}

export default function BookingPage() {
  const [customer_Id, setCustomerId] = useState<number | null>(null);
  const [booking, setBooking] = useState<Booking[]>([]);
  const [upcomingBookings, setUpcomingBookings] = useState<Booking[]>([]);
  const [olderBookings, setOlderBookings] = useState<Booking[]>([]);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const router = useRouter();

  useEffect(() => {
    const token = Cookies.get("AccessToken");
    if (token) {
      try {
        const decodedToken: DecodedToken = jwtDecode(token);
        const customerId = decodedToken.id;
        setCustomerId(customerId);
        setIsLoggedIn(true);
      } catch (error) {
        console.error("Failed to decode token:", error);
      }
    } else {
      router.push("/register/login");
    }
  }, []);

  useEffect(() => {
    const fetchBooking = async () => {
      if (customer_Id) {
        try {
          const response = await fetch(
            `http://localhost:3001/booking/${customer_Id}`
          );
          if (!response.ok) {
            throw new Error("Failed to fetch booking");
          }
          const data: Booking[] = await response.json();
          setBooking(data);
        } catch (error) {
          console.error("Error fetching booking:", error);
        }
      }
    };

    fetchBooking();
  }, [customer_Id]);

  useEffect(() => {
    const upcoming: Booking[] = [];
    const older: Booking[] = [];

    booking.forEach((book) => {
      if (isBefore(new Date(book.ticket.ticket_date), startOfToday())) {
        older.push(book);
      } else {
        upcoming.push(book);
      }
    });

    setUpcomingBookings(upcoming);
    setOlderBookings(older);
  }, [booking]);

  const handleCancelBooking = async (bookingId: number) => {
    const result = await Swal.fire({
      title: "Are you Sure?",
      text: "Do you want to cancel?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      confirmButtonText: "Yes, cancel it",
      cancelButtonText: "No, keep it",
    });
    if (result.isConfirmed) {
      try {
        const response = await fetch(
          `http://localhost:3001/booking/cancel/${bookingId}`,
          {
            method: "PUT",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({ bookingId }),
          }
        );

        if (!response.ok) {
          throw new Error("Failed to cancel booking");
        }
        Swal.fire("Cancelled!", "Your booking has been cancelled.", "success");
        setBooking((prevBookings) =>
          prevBookings.map((book) =>
            book.id === bookingId ? { ...book, status: "cancel" } : book
          )
        );
      } catch (error) {
        console.error("Error cancelling booking:", error);
        Swal.fire("Error!", "Failed to cancel booking.", "error");
      }
    }
  };
  if (!isLoggedIn) {
    return null;
  }

  const formatDate = (dateString: string) => {
    const date = new Date(dateString);
    return format(date, "do MMMM");
  };

  return (
    <>
      <div className="container mx-auto pt-4 mt-20 pb-4 justify-center bg-white w-11/12 md:w-3/4">
        <h2 className="font-bold text-lg text-center p-2">My Bookings</h2>
        <hr />
        {/* Display upcoming bookings */}
        <div className="py-4 p-2 bg-#f0f8ff">
          {upcomingBookings.length > 0 && (
            <>
              <h3 className="text-lg font-bold text-center text-pink-600">
                Upcoming Events
              </h3>
              {upcomingBookings.map((book) => (
                <div key={book.id}>
                  <div className="flex flex-col md:flex-row justify-around items-center">
                    <div className="w-full md:w-1/4 mb-4 md:mb-0">
                      <img
                        className="w-full md:w-40"
                        src={book.ticket.session.events.event_image}
                        alt={book.ticket.session.events.event_name}
                      />
                    </div>
                    <div className="w-full md:w-3/4">
                      <div className="flex flex-col md:flex-row justify-between mb-2">
                        <p className="font-bold text-left">
                          {book.ticket.session.events.event_name}
                        </p>
                        <p className="text-sm text-green-500 font-bold text-left">
                          Booked on: {formatDate(book.booking_date)}
                        </p>
                      </div>
                      <div className="text-sm">
                        <p>{book.ticket.session.session}</p>
                        <p>
                          {book.ticket.session.start_time} -{" "}
                          {book.ticket.session.end_time}
                        </p>
                      </div>
                      <div className="text-sm">
                        <p>Ticket: {book.ticket.ticket_name}</p>
                      </div>
                      <div className="text-sm">
                        <p>Persons: {book.no_of_persons}</p>
                      </div>

                      <div className="flex flex-col md:flex-row justify-between">
                        <div className="text-sm">
                          <p>Date: {book.ticket.ticket_date}</p>
                          <p>Price: {book.ticket.cost}</p>
                        </div>
                        <div>
                          {book.status !== "cancel" ? (
                            <div>
                                <Link
  href={`/confirm?bookingIds=${encodeURIComponent(JSON.stringify([book.id]))}`}
  type="button"
                                  className="focus:outline-none text-white bg-yellow-500 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg mx-2 text-sm px-5 py-2 mb-2"
                                >
                                  View Ticket
                                </Link>
                              <button
                                className="focus:outline-none text-white bg-rose-600 hover:bg-rose-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2 mb-2"
                                onClick={() => handleCancelBooking(book.id)}
                              >
                                Cancel
                              </button>
                            </div>
                          ) : (
                            <div>
                              <Link
                                href={`/details/${book.ticket.session.events.id}`}
                                passHref
                              >
                                <button
                                  type="button"
                                  className="focus:outline-none text-white bg-yellow-500 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg mx-2 text-sm px-5 py-2 mb-2"
                                >
                                  Rebook
                                </button>
                              </Link>
                              <button
                                className="focus:outline-none text-gray-400 bg-gray-200 cursor-not-allowed font-medium rounded-lg text-sm px-5 py-2 mb-2"
                                disabled
                              >
                                Cancelled
                              </button>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr />
                </div>
              ))}
            </>
          )}
        </div>

        {/* Older bookings */}
        <div>
          {olderBookings.length > 0 && (
            <>
              <h3 className="text-lg font-bold text-center text-pink-600">
                Older Bookings
              </h3>
              {olderBookings.map((book) => (
                <div key={book.id} className="py-2 bg-gray-100 p-2">
                  <div className="flex flex-col md:flex-row justify-around items-center">
                    <div className="w-full md:w-1/4 mb-4 md:mb-0">
                      <img
                        className="w-full md:w-44"
                        src={book.ticket.session.events.event_image}
                        alt={book.ticket.session.events.event_name}
                      />
                    </div>
                    <div className="w-full md:w-3/4">
                      <div className="flex flex-col md:flex-row justify-between mb-2">
                        <p className="font-bold text-left">
                          {book.ticket.session.events.event_name}
                        </p>
                        <p className="text-sm text-green-500 font-bold text-left">
                          Booked on: {formatDate(book.booking_date)}
                        </p>
                      </div>
                      <div className="text-sm">
                        <p>{book.ticket.session.session}</p>
                        <p>
                          {book.ticket.session.start_time} -{" "}
                          {book.ticket.session.end_time}
                        </p>
                      </div>
                      <div className="text-sm">
                        <p>Ticket: {book.ticket.ticket_name}</p>
                      </div>

                      <div className="flex flex-col md:flex-row justify-between">
                        <div className="text-sm">
                          <p>Date: {book.ticket.ticket_date}</p>

                          <p>Price: {book.ticket.cost}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr />
                </div>
              ))}
            </>
          )}
        </div>
      </div>
    </>
  );
}
