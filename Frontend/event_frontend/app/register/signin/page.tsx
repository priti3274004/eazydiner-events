"use client";
import React, { useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { useRouter } from "next/navigation";
import HomePage from "@/app/components/home/page";

interface IFormInput {
  name: string;
  email: string;
  contact: string;
  password: string;
  otp: string; // Add otp field to the form input
}

export default function SignInPage() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormInput>();
  const [serverError, setServerError] = useState<string | null>(null);
  const [showOTPField, setShowOTPField] = useState<boolean>(false); // State to toggle OTP field visibility
  const router = useRouter();

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    try {
      const response = await fetch("http://localhost:3001/signup", {
        method: "POST",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });
      const result = await response.json();

      if (response.ok) {
        // If server response indicates OTP is required, show the OTP field
        if (result.message === "OTP sent to contact. Please verify to complete registration.") {
          setShowOTPField(true);
        } else {
          // Redirect to the home page
          router.push("/");
        }
      } else {
        // Handle server-side validation error
        setServerError(result.message);
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  return (
    <>
      {/* <div className="fixed top-0 left-0 right-0">
        <HomePage />
      </div> */}
      <section>
        <div className="flex flex-col items-center justify-center md:mt-24 mt-40 lg:py-0">
          <div className="w-full bg-white rounded-lg md:mt-0 sm:max-w-md xl:p-0 ">
            <div className="p-6 space-y-4 md:space-y-6 sm:p-6">
              <h1 className="text-xl font-bold md:text-2xl text-center">
                Sign in to your account
              </h1>
              <form
                className="space-y-4 md:space-y-6"
                onSubmit={handleSubmit(onSubmit)}
              >
                <div>
                  <label className="block mb-2 text-sm font-medium text-gray-900">
                    Full Name
                  </label>
                  <input
                    type="text"
                    {...register("name", { required: "Full Name is required" })}
                    className="bg-gray-50 border border-gray-300 text-gray-900
                       rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5"
                    placeholder="John Doe"
                  />
                  {errors.name && (
                    <p className="text-red-600 text-sm mt-1">
                      {errors.name.message}
                    </p>
                  )}
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-gray-900">
                    Your email
                  </label>
                  <input
                    type="email"
                    {...register("email", {
                      required: "Please input your email",
                      pattern: {
                        value: /\S+@\S+\.\S+/,
                        message: "Entered value does not match email format",
                      },
                    })}
                    className="bg-gray-50 border border-gray-300 text-gray-900
                       rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5"
                    placeholder="name@company.com"
                  />
                  {errors.email && (
                    <p className="text-red-600 text-sm mt-1">
                      {errors.email.message}
                    </p>
                  )}
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-gray-900">
                    Contact
                  </label>
                  <input
                    type="tel"
                    {...register("contact", {
                      required: "Contact is required",
                    })}
                    className="bg-gray-50 border border-gray-300 text-gray-900
                       rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5"
                    placeholder="123-456-789"
                  />
                  {errors.contact && (
                    <p className="text-red-600 text-sm mt-1">
                      {errors.contact.message}
                    </p>
                  )}
                </div>
                {showOTPField && (
                  <div>
                    <label className="block mb-2 text-sm font-medium text-gray-900">
                      OTP
                    </label>
                    <input
                      type="text"
                      {...register("otp", {
                        required: "OTP is required",
                      })}
                      className="bg-gray-50 border border-gray-300 text-gray-900
                         rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5"
                      placeholder="Enter OTP"
                    />
                    {errors.otp && (
                      <p className="text-red-600 text-sm mt-1">
                        {errors.otp.message}
                      </p>
                    )}
                  </div>
                )}
                {serverError && (
                  <p className="text-red-600 text-sm mt-1">{serverError}</p>
                )}
                <button
                  type="submit"
                  className="w-full text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
                >
                  Sign in
                </button>
                <p className="text-sm font-light text-gray-500">
                  Already have an account?{" "}
                  <a
                    href={`/register/login`}
                    className="font-medium text-primary-600 hover:underline"
                  >
                    Login
                  </a>
                </p>
              </form>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
