"use client";
import React, { ChangeEvent, FormEvent, useState } from "react";
import { useRouter } from "next/navigation";
import HomePage from "@/app/components/home/page";

export default function SignInPage() {
  const [formData, setFormData] = useState({
    contact: "", // Changed from email to contact
    password: "",
    otp: "",
  });
  const [serverError, setServerError] = useState<string | null>(null);
  const [showOTPField, setShowOTPField] = useState(false); // State to toggle OTP field
  const router = useRouter();

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSendOTP = async () => {
    try {
      const response = await fetch("http://localhost:3001/signin", {
        method: "POST",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          contact: formData.contact,
        }),
      });
      const result = await response.json();

      if (response.ok) {
        setServerError(result.message);
        setShowOTPField(true); // Show OTP field after sending OTP
      } else {
        setServerError(result.message);
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    try {
      const response = await fetch("http://localhost:3001/signin", {
        method: "POST",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      });
      const result = await response.json();

      if (response.ok) {
        router.push("/");
        window.location.reload();
      } else {
        setServerError(result.message);
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  return (
    <>
      {/* <div className="fixed top-0 left-0 right-0">
        <HomePage />
      </div> */}
      <section>
        <div className="flex flex-col items-center justify-center md:mt-24 mt-40 lg:py-0">
          <div className="w-full bg-white rounded-lg md:mt-0 sm:max-w-md xl:p-0 ">
            <div className="p-6 space-y-4 md:space-y-6 sm:p-6">
              <h1 className="text-xl font-bold md:text-2xl text-center">
                Login to your account
              </h1>
              <form className="space-y-4 md:space-y-6" onSubmit={handleSubmit}>
                <div>
                  <label className="block mb-2 text-sm font-medium text-gray-900">
                    Your mobile number
                  </label>
                  <input
                    type="text"
                    name="contact"
                    id="contact"
                    className="bg-gray-50 border border-gray-300 text-gray-900
                       rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5"
                    placeholder="Enter your mobile number"
                    value={formData.contact}
                    onChange={handleChange}
                  />
                </div>
                {!showOTPField && (
                  <button
                    type="button"
                    className="w-full text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
                    onClick={handleSendOTP}
                  >
                    Get OTP
                  </button>
                )}
                {showOTPField && (
                  <div>
                    <label className="block mb-2 text-sm font-medium text-gray-900">
                      OTP
                    </label>
                    <input
                      type="text"
                      name="otp"
                      id="otp"
                      className="bg-gray-50 border border-gray-300 text-gray-900
                       rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5"
                      placeholder="Enter OTP"
                      value={formData.otp}
                      onChange={handleChange}
                    />
                  </div>
                )}
                {serverError && (
                  <p className="text-red-600 text-sm mt-1">{serverError}</p>
                )}
                <button
                  type="submit"
                  className="w-full text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
                >
                  {showOTPField ? "Login" : "Proceed"}
                </button>
                <p className="text-sm font-light text-gray-500">
                  Don’t have an account yet?{" "}
                  <a
                    href={`/register/signin`}
                    className="font-medium text-primary-600 hover:underline"
                  >
                    Signin
                  </a>
                </p>
              </form>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
