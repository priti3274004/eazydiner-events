"use client"
import { useRouter } from 'next/navigation';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store'; // Adjust the path as needed

const CityPage = () => {
  const router = useRouter();
  
  const { latitude, longitude, cityId } = useSelector((state: RootState) => state.location);
  
  if ( latitude === null || longitude === null || cityId === null) {
    return <div>Loading...</div>; // Or handle missing data appropriately
  }

  return (
    <div>
      {/* <h1>City: {cityname}</h1> */}
      <p>Selected City ID: {cityId}</p>
      <p>Latitude: {latitude}</p>
      <p>Longitude: {longitude}</p>
    </div>
  );
};

export default CityPage;
