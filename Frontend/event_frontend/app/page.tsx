"use client";
import React from 'react';
import Category from "./components/home/category";
import Image from "./components/home/banner";
import Music from "./components/home/music";
import Comedy from "./components/home/comedy";
import Dance from "./components/home/dance";
import Footer from "./components/footer";
import Today from "./components/home/today";
import EventExplorer from "./components/home/allevents";
import HomePage from './components/home/page';


import { Provider } from 'react-redux'
import store from "../redux/store";

export default function Home() {
  return (
    <Provider store={store}>
    <main>
    {/* <HomePage/> */}
      <Image />
      <Category />
      <Music />
      <Comedy />
      <Dance />
      <Today />     
      <EventExplorer />
      <Footer />
      
    </main>
     </Provider>
  );
}
