"use client";
import { useRouter } from "next/navigation";
import React, { useEffect, useState } from "react";
import HomePage from "../components/home/page";
import Footer from "../components/footer";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";
import Link from "next/link";
import { format } from "date-fns";

interface Event {
  id: number;
  name: string;
  date: string;
  location: string;
  description: string;
  icon: string;
  event_image: string;
  event_name: string;
  start_date: string;
}
const formatDate = (dateString: string) => {
  const date = new Date(dateString);
  return format(date, "do MMMM");
};

const weeklyEvents = () => {
  const [loading, setLoading] = useState(true);
  const [events, setEvents] = useState<Event[]>([]);
  const router = useRouter();
  const cityId = useSelector((state: RootState) => state.location.cityId);
  const latitude = useSelector((state: RootState) => state.location.latitude);
  const longitude = useSelector((state: RootState) => state.location.longitude);
  const location = useSelector((state: RootState) => state.location);
  useEffect(() => {
    const fetchData = async () => {
      try {
        let url = `http://localhost:3001/allevents/weeklyEvent?`;
        if (location.cityId) {
          url += `&city_id=${location.cityId}`;
        } else if (location.latitude && location.longitude) {
          url += `&latitude=${location.latitude}&longitude=${location.longitude}`;
        }

        const response = await fetch(url);
        if (!response.ok) {
          throw new Error("Failed to fetch data");
        }
        const data: Event[] = await response.json();
        console.log("Fetched events:", data); // Debugging line
        setEvents(data);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching data:", error);
        setLoading(false);
      }
    };

    fetchData();
  }, [cityId, latitude, longitude]);

  const handleClick = (id: number) => {
    router.refresh();
    router.push(`/details/${id}`);
  };

  return (
    <>
      <div>
        {/* <div className="fixed top-0 left-0 right-0 z-50">
          <HomePage />
        </div> */}
        <div className="flex flex-col items-center justify-center relative z-10">
          <div className="w-full md:max-w-full mt-20 my-4 bg-gradient-to-b flex items-center justify-center">
            <span className="text-xl font-bold text-white">Weekly Events</span>
          </div>
        </div>
        <section className="text-gray-100 body-font">
          <div className="container mx-auto">
            {!loading && events.length > 0 ? (
              <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
                {events.map((event) => (
                  <div
                    className="p-1 md:p-1 w-full cursor-pointer relative"
                    key={event.id}
                    onClick={() => handleClick(event.id)}
                  >
                    <Link href={`/details/${event.id}`} passHref>
                      <div className="p-1 md:p-1 w-full cursor-pointer relative">
                        <div className="h-44 md:h-full border-2 border-gray-200 border-opacity-10 rounded-lg overflow-hidden">
                          <div className="relative">
                            <img
                              className="h-44 md:h-40 w-full object-cover object-center"
                              src={event.event_image}
                              alt={event.event_name}
                            />
                            <div className="absolute inset-0 bg-gradient-to-t from-black/90 via-transparent to-transparent"></div>
                          </div>
                          <div className="absolute bottom-0 w-full text-white p-4 ">
                            <div>
                              <h4 className="text-sm font-medium">
                                {event.event_name}
                              </h4>
                            </div>
                            <div className="flex justify-between">
                              <div className="text-xs font-medium text-gray-400 mt-1">
                                {formatDate(event.start_date)}
                              </div>
                              <button
                                type="button"
                                className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-red-300 font-medium rounded-sm text-xs p-1 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
                              >
                                Book Tickets
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Link>
                  </div>
                ))}
              </div>
            ) : (
              <p>Events Comming Soon</p>
            )}
          </div>
        </section>
      </div>
      <Footer />
    </>
  );
};
export default weeklyEvents;
