"use client";
import React, { useEffect, useState } from "react";
import { useSearchParams } from "next/navigation";

interface BookingDetails {
  id: number;
  name: string;
  contact: number;
  no_of_persons: number;
  qr_code: string;
  custom_bookingId: string;
  ticket: {
    ticket_id: number;
    ticket_name: string;
    session: {
      session: string;
      events: {
        event_name: string;
      };
    };
  };
}
const ConfirmPage = () => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const [bookings, setBookings] = useState<BookingDetails[]>([]);
  const searchParams = useSearchParams();

  useEffect(() => {
    const fetchBookings = async () => {
      try {
        const bookingIdsParam = searchParams?.get("bookingIds") || null;
        if (!bookingIdsParam) {
          throw new Error("No booking IDs found in query parameters.");
        }
        const ids = JSON.parse(bookingIdsParam as string);

        const bookingPromises = ids.map(async (id: number) => {
          const response = await fetch(
            `http://localhost:3001/booking/id/${id}`
          );
          if (!response.ok) {
            throw new Error(`Failed to fetch booking with ID ${id}`);
          }
          return response.json();
        });
        const results = await Promise.all(bookingPromises);
        const bookingsData = results.map((result) => result.booking);
        setBookings(bookingsData);
      } catch (error) {
        setError(`Error fetching booking details`);
      } finally {
        setLoading(false);
      }
    };

    fetchBookings();
  }, [searchParams]);

  if (loading) {
    return <div>Loading booking details...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <>
      <div className="container mx-auto pt-12">
        <div className="container mx-auto flex justify-center items-center pt-12 h-9">
          <img
            className="h-12"
            src="https://cdn-icons-png.freepik.com/256/14025/14025690.png"
          />
        </div>

        <div className="container mx-auto pt-12 pb-4 justify-center bg-white w-full md:w-2/5">
          <h2 className="font-bold text-lg text-center text-rose-500">
            Thank You for Booking with Us!!
          </h2>
          <h3 className="text-sm text-center">Your booking is confirmed</h3>
          {bookings.length > 0 && (
            <div className="mb-1">
              <div className="text-sm flex justify-center">
                <img src={bookings[0].qr_code}></img>
              </div>
              <p className="text-gray-700 text-center">Ticket Id:</p>
              <p className="font-bold text-green-700 text-center">
                {bookings[0].custom_bookingId}
              </p>

              <div className="text-sm flex justify-between">
                <p className="font-bold">Name:</p>
                <p>{bookings[0].name}</p>
              </div>
              <div className="text-sm flex justify-between">
                <p className="font-bold">Contact:</p>
                <p>{bookings[0].contact}</p>
              </div>
            </div>
          )}
          <hr></hr>

          {bookings.map((booking) => (
            <div key={booking.id} className="py-1">
              <div className="text-sm flex justify-between">
                <p className="font-bold">No of Persons:</p>
                <p> {booking.no_of_persons}</p>
              </div>
              <div className="text-sm flex justify-between">
                <p className="font-bold">Event Name:</p>
                <p> {booking.ticket.session.events.event_name}</p>
              </div>
              <div className="text-sm flex justify-between">
                <p className="font-bold">Session :</p>
                <p> {booking.ticket.session.session}</p>
              </div>
              <div className="text-sm flex justify-between">
                <p className="font-bold">Ticket Name:</p>
                <p> {booking.ticket.ticket_name}</p>
              </div>
              <hr></hr>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default ConfirmPage;
