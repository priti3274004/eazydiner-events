"use client";
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/store";
import Link from "next/link";
import Footer from "../components/footer";
import { useRouter } from "next/navigation";
import { format } from "date-fns";
import HomePage from "../components/home/page";

interface AllEvents {
  id: number;
  event_name: string;
  event_image: string;
  start_date: string;
  category_id: number;
}

const formatDate = (dateString: string) => {
  const date = new Date(dateString);
  return format(date, "do MMMM");
};

export const viewEvents = () => {
  const [events, setEvents] = useState<AllEvents[]>([]);
  const location = useSelector((state: RootState) => state.location);
  const [loading, setLoading] = useState(true);
  const router = useRouter();
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const limit = 12;

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        let url = `http://localhost:3001/allevents?limit=${limit}&offset=${
          (page - 1) * limit
        }`;
        if (location.cityId) {
          url += `&city_id=${location.cityId}`;
        }
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error("Failed to fetch data");
        }
        const data = await response.json();
        setEvents(data.events);
        setTotalPages(data.totalPages);
      } catch (error) {
        console.error("Error fetching data:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [location.cityId, page]);

  const handleClick = (id: number) => {
    router.push(`/details/${id}`);
  };
  const handlePageChange = (newPage: number) => {
    setPage(newPage);
  };
  return (
    <>
      <div>
        {/* <div className="fixed top-0 left-0 right-0 z-50">
          <HomePage />
        </div> */}
        <div className="flex flex-col items-center justify-center relative z-10">
          <div className="w-full md:max-w-full mt-20 my-4 bg-gradient-to-b flex items-center justify-center">
            <span className="text-xl font-bold text-white">All Events</span>
          </div>
        </div>

        <section className="text-gray-100 body-font">
          <div className="container mx-auto">
            {!loading && events.length > 0 ? (
              <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
                {events.map((event) => (
                  <div
                    className="p-1 md:p-1 w-full cursor-pointer relative"
                    key={event.id}
                    onClick={() => handleClick(event.id)}
                  >
                    <Link href={`/details/${event.id}`} passHref>
                      <div className="p-1 md:p-1 w-full cursor-pointer relative">
                        <div className="h-44 md:h-full border-2 border-gray-200 border-opacity-10 rounded-lg overflow-hidden">
                          <div className="relative">
                            <img
                              className="h-44 md:h-40 w-full object-cover object-center"
                              src={event.event_image}
                              alt={event.event_name}
                            />
                            <div className="absolute inset-0 bg-gradient-to-t from-black/90 via-transparent to-transparent"></div>
                          </div>
                          <div className="absolute bottom-0 w-full text-white p-4 ">
                            <div>
                              <h4 className="text-sm font-medium">
                                {event.event_name}
                              </h4>
                            </div>
                            <div className="flex justify-between">
                              <div className="text-xs font-medium text-gray-400 mt-1">
                                {formatDate(event.start_date)}
                              </div>
                              <button
                                type="button"
                                className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-red-300 font-medium rounded-sm text-xs p-1 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
                              >
                                Book Tickets
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Link>
                  </div>
                ))}
              </div>
            ) : (
              <p>Events Comming Soon</p>
            )}
            <div className="flex justify-center mt-4">
              {Array.from({ length: totalPages }, (_, index) => (
                <button
                  key={index}
                  className={`mx-1 px-3 py-1 rounded ${
                    page === index + 1
                      ? "bg-rose-500 text-white"
                      : "bg-gray-200 text-gray-700"
                  }`}
                  onClick={() => handlePageChange(index + 1)}
                >
                  {index + 1}
                </button>
              ))}
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </>
  );
};

export default viewEvents;
