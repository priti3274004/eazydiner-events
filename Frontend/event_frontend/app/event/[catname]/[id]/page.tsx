"use client";
import { useParams, useRouter } from "next/navigation";
import React, { useEffect, useState } from "react";
import { format } from "date-fns";
import Footer from "../../../components/footer";
import Link from "next/link";
import { useSelector } from "react-redux"; // Import useSelector
import { RootState } from "../../../../redux/store";

interface Event {
  id: number;
  name: string;
  date: string;
  location: string;
  description: string;
  icon: string;
  event_image: string;
  event_name: string;
  start_date: string;
}

const formatDate = (dateString: string) => {
  const date = new Date(dateString);
  return format(date, "do MMMM");
};

const Events = () => {
  const [loading, setLoading] = useState(true); // Add a loading state
  const [events, setEvents] = useState<Event[]>([]);
  const { id }: any = useParams();
  const router = useRouter();
  const [categoryIcon, setCategoryIcon] = useState<string | null>(null);
  const [categoryName, setCategoryName] = useState<string>("");
  const { cityId, latitude, longitude } = useSelector((state: RootState) => state.location); // Destructure location state
  const location = useSelector((state: RootState) => state.location);
  useEffect(() => {
    if (id) {
      const fetchCategoryAndEvents = async () => {
        try {
          // Fetch category details to get the icon and name
          const categoryResponse = await fetch(
            `http://localhost:3001/getcategory/${id}`
          );
          if (!categoryResponse.ok) {
            throw new Error("Failed to fetch category");
          }
          const categoryData = await categoryResponse.json();
          setCategoryIcon(categoryData.icon);
          setCategoryName(categoryData.name); // Assuming category name is in `name` field

          // Build API URL based on available parameters
          let apiUrl = `http://localhost:3001/allevents?category_id=${id}`;
          if (location.cityId) {
            apiUrl += `&city_id=${location.cityId}`;
          } else if (location.latitude && location.longitude) {
            apiUrl += `&latitude=${location.latitude}&longitude=${location.longitude}`;
          }

          // Fetch events based on category_id, city_id, and/or latitude/longitude
          const eventsResponse = await fetch(apiUrl);
          if (!eventsResponse.ok) {
            throw new Error("Failed to fetch events");
          }
          const eventData = await eventsResponse.json();
          setEvents(eventData.events);
        } catch (error) {
          console.error("Error fetching data:", error);
        } finally {
          setLoading(false);
        }
      };

      fetchCategoryAndEvents();
    }
  }, [id, cityId, latitude, longitude]);

  if (!id) {
    return <div>Loading...</div>;
  }

  const handleClick = (id: number) => {
    router.push(`/details/${id}`);
  };
  return (
    <>
      <section className="text-gray-100 body-font">
        <div className="container mx-auto">
          {categoryIcon && (
            <div className="flex flex-col items-center justify-center relative z-10">
              <div className="w-full md:max-w-full mt-20 my-4 bg-gradient-to-b flex items-center justify-center">
                <span className="text-xl font-bold text-white">
                  {categoryName}
                </span>
              </div>
            </div>
          )}
          {!loading && events.length > 0 ? (
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
              {events.map((event) => (
                <div
                  className="p-1 md:p-1 w-full cursor-pointer relative"
                  key={event.id}
                  onClick={() => handleClick(event.id)}
                >
                  <Link href={`/details/${event.id}`} passHref>
                    <div className="p-1 md:p-1 w-full cursor-pointer relative">
                      <div className="h-44 md:h-full border-2 border-gray-200 border-opacity-10 rounded-lg overflow-hidden">
                        <div className="relative">
                          <img
                            className="h-44 md:h-40 w-full object-cover object-center"
                            src={event.event_image}
                            alt={event.event_name}
                          />
                          <div className="absolute inset-0 bg-gradient-to-t from-black/90 via-transparent to-transparent"></div>
                        </div>
                        <div className="absolute bottom-0 w-full text-white p-4 ">
                          <div>
                            <h4 className="text-sm font-medium">
                              {event.event_name}
                            </h4>
                          </div>
                          <div className="flex justify-between">
                            <div className="text-xs font-medium text-gray-400 mt-1">
                              {formatDate(event.start_date)}
                            </div>
                            <button
                              type="button"
                              className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-red-300 font-medium rounded-sm text-xs p-1 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
                            >
                              Book Tickets
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Link>
                </div>
              ))}
            </div>
          ) : (
            <p>Events Comming Soon</p>
          )}
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Events;
