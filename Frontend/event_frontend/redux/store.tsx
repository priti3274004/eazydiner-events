"use client";
import { configureStore } from '@reduxjs/toolkit';
import { persistStore, persistReducer } from 'redux-persist';
import createWebStorage from 'redux-persist/lib/storage/createWebStorage';
import locationReducer from './slice/locationslice';
import visibilityReducer from './slice/visibleslice';

// Noop storage for SSR environments
const createNoopStorage = () => {
  return {
    getItem(_key: string) {
      return Promise.resolve(null);
    },
    setItem(_key: string, value: string) {
      return Promise.resolve(value);
    },
    removeItem(_key: string) {
      return Promise.resolve();
    },
  };
};

const storage = typeof window !== 'undefined' ? createWebStorage('local') : createNoopStorage();
const persistConfig = {
  key: 'root',
  storage,
};

const persistedLocationReducer = persistReducer(persistConfig, locationReducer);
const persistedVisibilityReducer = persistReducer(persistConfig, visibilityReducer);

const store = configureStore({
  reducer: {
    location: persistedLocationReducer,
    visibility: persistedVisibilityReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: ['persist/PERSIST', 'persist/REHYDRATE'],
      },
    }),
});

export const persistor = persistStore(store);
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
