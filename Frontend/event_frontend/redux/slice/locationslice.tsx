"use client"
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface LocationState {
  latitude?: number | null;
  longitude?: number | null;
  cityId?: number | null; 
}

const initialState: LocationState = {
  latitude: null,
  longitude: null,
  cityId: 2, 
};

const locationSlice = createSlice({
  name: 'location',
  initialState,
  reducers: {
    setLocation(state, action: PayloadAction<{ latitude?: number; longitude?: number; cityId?: number }>) {
      state.latitude = action.payload.latitude;
      state.longitude = action.payload.longitude;
      if (action.payload.cityId !== undefined) {
        state.cityId = action.payload.cityId; 
       
      }
    },
    clearLocation(state) {
      state.latitude = null;
      state.longitude = null;
      state.cityId = null;
    },
  },
});

export const {setLocation, clearLocation } = locationSlice.actions;
export default locationSlice.reducer;
