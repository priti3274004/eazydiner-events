import { createSlice } from '@reduxjs/toolkit';

const visibilitySlice = createSlice({
  name: 'visibility',
  initialState: {
    under10KmVisible: false,
  },
  reducers: {
    showUnder10Km: (state) => {
      state.under10KmVisible = true;
    },
    hideUnder10Km: (state) => {
      state.under10KmVisible = false;
    },
  },
});

export const { showUnder10Km, hideUnder10Km } = visibilitySlice.actions;
export default visibilitySlice.reducer;
