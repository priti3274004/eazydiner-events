// utils/eventUtils.js
const { haversineDistance } = require('./distance');

const filterAndMapEventsByDistance = (latitude, longitude, maxDistance, restaurants) => {
  return restaurants
    .filter(restaurant => {
      const distance = haversineDistance(latitude, longitude, restaurant.latitude, restaurant.longitude);
      return distance <= maxDistance;
    })
    .map(restaurant =>
      restaurant.events.map(event => ({
        ...event.get({ plain: true }),
        distance: haversineDistance(latitude, longitude, restaurant.latitude, restaurant.longitude)
      }))
    )
    .flat();
};

module.exports = { filterAndMapEventsByDistance };
