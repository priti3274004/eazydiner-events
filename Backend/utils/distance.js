// utils/distance.js
const haversineDistance = (lat1, lon1, lat2, lon2) => {
  const toRadians = angle => angle * (Math.PI / 180.0);

  lat1 = toRadians(lat1);
  lon1 = toRadians(lon1);
  lat2 = toRadians(lat2);
  lon2 = toRadians(lon2);

  const dLat = lat2 - lat1;
  const dLon = lon2 - lon1;

  const a = Math.pow(Math.sin(dLat / 2), 2) +
            Math.cos(lat1) * Math.cos(lat2) *
            Math.pow(Math.sin(dLon / 2), 2);
  const rad = 6371; // Radius of the Earth in kilometers
  const c = 2 * Math.asin(Math.sqrt(a));
  return rad * c;
};

module.exports = { haversineDistance };
