const request = require('supertest');
const express = require('express');
const bodyParser = require('body-parser');
const { loginUser } = require('../Controllers/customerController');

jest.mock('../models', () => require('../__mocks__/models'));

jest.mock('../Controllers/otpgenerator', () => ({
  generateOTP: jest.fn(() => '1234'),
}));
jest.mock('jsonwebtoken', () => ({
  sign: jest.fn(() => 'mocked_jwt_token'),
}));

const app = express();
app.use(bodyParser.json());

app.post('/signin', loginUser);

describe('POST /signin', () => {
  it('should send OTP and return success message if user exists', async () => {
    const res = await request(app)
      .post('/signin')
      .send({ contact: '9816797667' })
      .expect(200);

    expect(res.body.message).toBe('OTP sent to contact. Please verify to login.');
  });
});
