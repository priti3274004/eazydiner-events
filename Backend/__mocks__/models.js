const SequelizeMock = require('sequelize-mock');
const dbMock = new SequelizeMock();

const CustomerMock = dbMock.define('customer', {
  id: 1,
  contact: '9816797667',
  otp: null,
  otpExpiresAt: null,
  name: 'John Doe',
  email: 'john.doe@example.com',
  createdAt: new Date(),
});

module.exports = {
  Sequelize: dbMock,
  Customer: CustomerMock,
};
