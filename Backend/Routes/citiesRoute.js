const express = require('express');
const router = express.Router();
const cityController = require('../Controllers/citiesController');

// Route to fetch all cities
router.get('/cities', cityController.getAllCities);

module.exports = router;
