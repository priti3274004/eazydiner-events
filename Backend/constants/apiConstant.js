const COOKIE_OPTIONS = {
  httpOnly: false,
  secure: true,
  sameSite: "none",
  maxAge: 3600000, 
};

const CORS_OPTIONS = {
  origin: ["http://localhost:3000", "http://127.0.0.1:3000"],
  credentials: true,
  methods: ["GET", "PUT", "POST", "DELETE", "OPTIONS"],
};

module.exports = { COOKIE_OPTIONS, CORS_OPTIONS };
