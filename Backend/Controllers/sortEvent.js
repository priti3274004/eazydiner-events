const db = require("../models");
const Event = db.events;
const Ticket = db.ticket;
const Session = db.session;
const City = db.City;
const { haversineDistance } = require("../utils/distance");
const {restaurant, events} = require("../models");
const { filterAndMapEventsByDistance } = require("../utils/eventutil");

const eventDate = async (req, res) => {
  try {
    const { city_id, latitude, longitude } = req.query;
    const maxDistance = req.query.maxDistance || 20; // Default max distance to 20 km
    let results;

    if (city_id) {
      results = await Event.findAll({
        where: { city_id },
        order: [["start_date", "ASC"]],
      });
    } else if (latitude && longitude) {
      const restaurants = await restaurant.findAll({
        include: {
          model: events,
          as: "events",
          required: true,
        },
      });

      const nearbyEvents = filterAndMapEventsByDistance(
        parseFloat(latitude),
        parseFloat(longitude),
        maxDistance,
        restaurants
      );

      results = nearbyEvents.sort((a, b) => new Date(a.start_date) - new Date(b.start_date));
    } else {
      results = await Event.findAll({
        order: [["start_date", "ASC"]],
      });
    }

    res.status(200).json(results);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Error while sorting events on the basis of date" });
  }
};
const eventCost = async (req, res) => {
  try {
    const { sort, city_id, latitude, longitude } = req.query;
    const maxDistance = req.query.maxDistance || 20; // Default max distance to 20 km

    let order;
    if (sort === "asc") {
      order = [["session", "ticket", "display_price", "ASC"]];
    } else if (sort === "desc") {
      order = [["session", "ticket", "display_price", "DESC"]];
    } else {
      order = [["start_date", "ASC"]]; // Default order if sort is not provided
    }

    let eventsList;

    if (city_id) {
      eventsList = await Event.findAll({
        where: { city_id },
        include: [
          {
            model: Session,
            as: "session",
            include: [
              {
                model: Ticket,
                as: "ticket",
                attributes: ["display_price"],
              },
            ],
          },
        ],
        order: order,
      });
    } else if (latitude && longitude) {
      const restaurants = await restaurant.findAll({
        include: {
          model: events,
          as: "events",
          required: true,
          include: [
            {
              model: Session,
              as: "session",
              include: [
                {
                  model: Ticket,
                  as: "ticket",
                  attributes: ["display_price"],
                },
              ],
            },
          ],
        },
      });

      console.log("Restaurants found:", restaurants.length);

      let nearbyEvents = filterAndMapEventsByDistance(
        parseFloat(latitude),
        parseFloat(longitude),
        maxDistance,
        restaurants
      );

      console.log("Nearby events found before sorting:", nearbyEvents.length);

      // Sort nearby events by display price based on the sort parameter
      if (sort === "asc") {
        nearbyEvents = nearbyEvents.sort((a, b) => {
          if (a.ticket && b.ticket) {
            return a.ticket.display_price - b.ticket.display_price;
          } else {
            return 0; // Keep events without tickets in place
          }
        });
      } else if (sort === "desc") {
        nearbyEvents = nearbyEvents.sort((a, b) => {
          if (a.ticket && b.ticket) {
            return b.ticket.display_price - a.ticket.display_price;
          } else {
            return 0; // Keep events without tickets in place
          }
        });
      }

      console.log("Nearby events after sorting:", nearbyEvents.length);

      eventsList = nearbyEvents;
    } else {
      eventsList = await Event.findAll({
        include: [
          {
            model: Session,
            as: "session",
            include: [
              {
                model: Ticket,
                as: "ticket",
                attributes: ["display_price"],
              },
            ],
          },
        ],
        order: order,
      });
    }

    res.status(200).json(eventsList);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Error while sorting events" });
  }
};

module.exports = { eventCost, eventDate };
