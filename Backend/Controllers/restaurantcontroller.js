
const { Op } = require('sequelize');
const { restaurant, events, City } = require('../models');
const { haversineDistance } = require('../utils/distance');
const { filterAndMapEventsByDistance } = require('../utils/eventutil');

let userLocation = { latitude: null, longitude: null };

const getNearbyEvents = async (req, res) => {
  const { latitude, longitude } = userLocation;
  console.log(`Current stored location: ${latitude}, ${longitude}`);
  if (!latitude || !longitude) {
    return res.status(400).json({ error: 'User location not available' });
  }

  const maxDistance = req.query.maxDistance || 10;

  try {
    const restaurants = await restaurant.findAll({
      include: {
        model: events,
        as: 'events',
        required: true
      }
    });

    const nearbyEvents = filterAndMapEventsByDistance(latitude, longitude, maxDistance, restaurants);

    res.json(nearbyEvents);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred while fetching events' });
  }
};

const getNearbyEventsSortedByDistance = async (req, res) => {
  const { latitude, longitude } = userLocation;
  console.log(`Current stored location: ${latitude}, ${longitude}`);
  if (!latitude || !longitude) {
    return res.status(400).json({ error: 'User location not available' });
  }

  const maxDistance = req.query.maxDistance || 10;

  try {
    const restaurants = await restaurant.findAll({
      include: {
        model: events,
        as: 'events',
        required: true
      }
    });

    const nearbyEvents = filterAndMapEventsByDistance(latitude, longitude, maxDistance, restaurants);
    nearbyEvents.sort((eventA, eventB) => eventA.distance - eventB.distance);

    res.json(nearbyEvents);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred while fetching events' });
  }
};

const updateLocation = (req, res) => {
  const { latitude, longitude } = req.body;

  if (latitude && longitude) {
    userLocation = { latitude, longitude };
    console.log(`Received location update: ${latitude}, ${longitude}`);
    getNearbyEvents(req, res);
  } else {
    userLocation = { latitude: 28.62700000, longitude: 77.20450000 };
    console.log(`Using default location: ${userLocation.latitude}, ${userLocation.longitude}`);
  }
};

const getUserLocation = () => {
  console.log(`Returning stored location: ${userLocation.latitude}, ${userLocation.longitude}`);
  return userLocation;
};

module.exports = { getNearbyEvents, updateLocation, getUserLocation, getNearbyEventsSortedByDistance, haversineDistance };
