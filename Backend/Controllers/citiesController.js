const db = require('../models'); // Assuming your Sequelize models are in the 'models' directory

// Fetch all cities
exports.getAllCities = async (req, res) => {
  try {
    const cities = await db.City.findAll();
    res.json(cities);
  } catch (error) {
    console.error('Error fetching cities:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};
