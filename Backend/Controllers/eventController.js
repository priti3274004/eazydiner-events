const db = require("../models");
const { Op } = require("sequelize");
const Event = db.events;
const City = db.City;
const Session = db.session;
const Category = db.event_category;
const moment = require("moment");
const Ticket = db.ticket;
const redis = require("../utils/redis");
const {restaurant, events} = require("../models");
const {haversineDistance} = require("../utils/distance");
const {filterAndMapEventsByDistance} = require("../utils/eventutil")

const getEvents = async (req, res) => {
  const { latitude, longitude } = req.query; // Fetch latitude and longitude from the request query
  const maxDistance = req.query.maxDistance || 20;

  try {
    const {
      id,
      event_name,
      category_id,
      search,
      event_category,
      limit,
      offset,
      city_id, // Parameter for city id from API request
      tags, // New parameter for tags
    } = req.query;

    // Create a unique cache key based on the request parameters
    const cacheKey = `events_${id || ""}_${event_name || ""}_${category_id || ""}_${event_category || ""}_${search || ""}_${limit || "default"}_${offset || "default"}_${city_id || "all"}_${tags || ""}_${latitude || "no_lat"}_${longitude || "no_lon"}`;

    // Try to get cached data
    const cachedData = await redis.get(cacheKey);
    if (cachedData) {
      const parsedData = JSON.parse(cachedData);
      return res.status(200).send(parsedData);
    }

    // Define the common query options
    const queryOptions = {
      where: {
        end_date: { [Op.gte]: new Date() }, // Ensure events are not past
      },
      include: [
        {
          model: City,
          as: "city",
          attributes: ["name"],
        },
      ],
    };

    // Apply filters based on parameters
    if (id) {
      queryOptions.where.id = id;
    }

    if (event_name) {
      queryOptions.where.event_name = event_name;
    }

    if (category_id) {
      queryOptions.where.category_id = category_id;
    }

    if (event_category) {
      queryOptions.include.push({
        model: Category,
        as: "category",
        where: { name: event_category },
      });
    }

    if (search) {
      queryOptions.where[Op.or] = [
        { event_name: { [Op.like]: `%${search}%` } },
        { event_description: { [Op.like]: `%${search}%` } },
      ];
    }

    if (city_id) {
      queryOptions.where.city_id = city_id;
    }

    if (tags) {
      queryOptions.where.tags = { [Op.like]: `%${tags}%` };
    }

    if (limit && !isNaN(parseInt(limit, 10))) {
      queryOptions.limit = parseInt(limit, 10);
    }

    if (offset && !isNaN(parseInt(offset, 10))) {
      queryOptions.offset = parseInt(offset, 10);
    }

    // Fetch results based on query options
    const results = await events.findAndCountAll(queryOptions);

    let eventsList = results.rows.map(event => event.get({ plain: true }));

    if (latitude && longitude) {
      const restaurants = await restaurant.findAll({
        include: {
          model: events,
          as: 'events',
          required: true
        }
      });

      const nearbyEvents = restaurants
        .filter(restaurant => {
          const distance = haversineDistance(latitude, longitude, restaurant.latitude, restaurant.longitude);
          return distance <= maxDistance;
        })
        .map(restaurant => {
          return restaurant.events.map(event => ({
            ...event.get({ plain: true }),
            distance: haversineDistance(latitude, longitude, restaurant.latitude, restaurant.longitude)
          }));
        })
        .flat();

      eventsList = eventsList.filter(event => nearbyEvents.find(neEvent => neEvent.id === event.id));
      eventsList = eventsList.map(event => {
        const neEvent = nearbyEvents.find(neEvent => neEvent.id === event.id);
        return {
          ...event,
          distance: neEvent.distance
        };
      });

      eventsList.sort((a, b) => a.distance - b.distance);
    }

    const totalPages = Math.ceil(results.count / (limit ? parseInt(limit, 10) : 12));

    const responseData = {
      events: eventsList,
      totalPages: totalPages,
    };

    // Cache and return results
    await redis.set(cacheKey, JSON.stringify(responseData), "EX", 3600);
    return res.status(200).send(responseData);
  } catch (error) {
    console.error("Error handling request:", error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

const EventsthisWeek = async (req, res) => {
  try {
    const { city_id, latitude, longitude } = req.query;
    const today = new Date();
    const day = today.getDay();
    const diff = today.getDate() - day + (day === 0 ? -6 : 1);

    const startOfWeek = new Date(today.setDate(diff));
    startOfWeek.setHours(0, 0, 0, 0);
    const endOfWeek = new Date(today.setDate(diff + 6));
    endOfWeek.setHours(23, 59, 59, 999);

    const cacheKey = `eventsthisweek_${city_id || "all"}_${latitude || "no_lat"}_${longitude || "no_lon"}`;
    const cachedData = await redis.get(cacheKey);

    if (cachedData) {
      const parsedData = JSON.parse(cachedData);
      return res.status(200).send(parsedData);
    }

    // Construct the `where` condition dynamically
    const whereCondition = {
      [Op.and]: [
        {
          start_date: {
            [Op.lte]: endOfWeek,
          },
        },
        {
          [Op.or]: [
            { end_date: { [Op.gte]: startOfWeek } },
            { end_date: null }, // Include events that do not have an end date
          ],
        },
      ],
    };

    if (city_id) {
      whereCondition.city_id = city_id;
    }

    const weeklyEvents = await Event.findAll({
      include: [
        {
          model: Session,
          as: "session",
          required: true,
          include: [
            {
              model: Ticket,
              as: "ticket",
              required: true,
              where: {
                ticket_date: {
                  [Op.between]: [startOfWeek, endOfWeek],
                },
              },
            },
          ],
        },
      ],
      where: whereCondition,
    });

    let eventsList = weeklyEvents.map(event => event.get({ plain: true }));

    if (latitude && longitude) {
      const restaurants = await restaurant.findAll({
        include: {
          model: Event,
          as: 'events',
          required: true,
        },
      });

      const nearbyEvents = restaurants
        .filter(restaurant => {
          const distance = haversineDistance(latitude, longitude, restaurant.latitude, restaurant.longitude);
          return distance <= 20; 
        })
        .map(restaurant => {
          return restaurant.events.map(event => ({
            ...event.get({ plain: true }),
            distance: haversineDistance(latitude, longitude, restaurant.latitude, restaurant.longitude)
          }));
        })
        .flat();

      eventsList = eventsList.filter(event => nearbyEvents.find(neEvent => neEvent.id === event.id));
      eventsList = eventsList.map(event => {
        const neEvent = nearbyEvents.find(neEvent => neEvent.id === event.id);
        return {
          ...event,
          distance: neEvent.distance
        };
      });

      eventsList.sort((a, b) => a.distance - b.distance);
    }

    await redis.set(cacheKey, JSON.stringify(eventsList), "EX", 3600);
    return res.status(200).json(eventsList);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error while fetching events" });
  }
};

const getNightEvents = async (req, res) => {
  try {
    const { city_id } = req.query;
    const lateTime = "19:00:00";
    const cacheKey = "Night Events";
    const cachedData = await redis.get(cacheKey);

    if (cachedData) {
      const parsedData = JSON.parse(cachedData);
      return res.status(200).send(parsedData);
    }
    const results = await Event.findAll({
      include: {
        model: Session,
        as: "session",
        where: {
          start_time: {
            [Op.gt]: lateTime,
          },
        },
      },
      where: { city_id },
    });
    await redis.set(cacheKey, JSON.stringify(results), "EX", 3600);
    return res.status(200).json(results);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error while fetching sessions" });
  }
};

const getTodayEvent = async (req, res) => {
  try {
    const { city_id, latitude, longitude } = req.query;
    const today = new Date();
    const formattedDate = moment(today).format("YYYY-MM-DD");
    const cacheKey = `todayEvents_${city_id || "all"}_${latitude || "no_lat"}_${longitude || "no_lon"}`;
    const cachedData = await redis.get(cacheKey);

    if (cachedData) {
      const parsedData = JSON.parse(cachedData);
      return res.status(200).send(parsedData);
    }

    // Construct the `where` condition dynamically
    const whereCondition = city_id ? { city_id } : {};

    const results = await Event.findAll({
      include: {
        model: Session,
        as: "session",
        required: true,
        include: {
          model: Ticket,
          as: "ticket",
          where: {
            ticket_date: {
              [Op.eq]: formattedDate,
            },
          },
          required: true,
        },
      },
      where: whereCondition,
    });

    let eventsList = results.map(event => event.get({ plain: true }));

    if (latitude && longitude) {
      const restaurants = await restaurant.findAll({
        include: {
          model: Event,
          as: 'events',
          required: true,
        },
      });

      const nearbyEvents = restaurants
        .filter(restaurant => {
          const distance = haversineDistance(latitude, longitude, restaurant.latitude, restaurant.longitude);
          return distance <= 20; // default max distance of 20 km
        })
        .map(restaurant => {
          return restaurant.events.map(event => ({
            ...event.get({ plain: true }),
            distance: haversineDistance(latitude, longitude, restaurant.latitude, restaurant.longitude)
          }));
        })
        .flat();

      eventsList = eventsList.filter(event => nearbyEvents.find(neEvent => neEvent.id === event.id));
      eventsList = eventsList.map(event => {
        const neEvent = nearbyEvents.find(neEvent => neEvent.id === event.id);
        return {
          ...event,
          distance: neEvent.distance
        };
      });

      eventsList.sort((a, b) => a.distance - b.distance);
    }

    await redis.set(cacheKey, JSON.stringify(eventsList), "EX", 3600);
    return res.status(200).json(eventsList);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error while fetching events" });
  }
};

const getEventsbyId = async (req, res) => {
  let id = req.params.id;
  let event = await Event.findOne({
    where: {
      id: id,
    },
  });
  res.status(200).send(event);
};

const createEvent = async (req, res) => {
  try {
    const newEvent = await Event.create(req.body);
    res.status(201).json({ newEvent });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error during insertion" });
  }
};
const newEvent = async (req, res) => {
  try {
    const today = new Date();
    const newEvent = today.setDate(today.getDate() - 2);
    const formattedDate = moment(newEvent).format("YYYY-MM-DD");

    console.log("newEvent: ", formattedDate);
    const result = await Event.findAll({
      where: {
        createdAt: {
          [Op.gt]: formattedDate,
        },
      },
    });
    res.status(200).send(result);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error while fetching sessions" });
  }
};

const deleteEvent = async (req, res) => {
  try {
    const event = await Event.findByPk(req.params.id);
    if (!event) {
      return res.status(404).json({ error: "Event not found" });
    }
    await event.destroy();
    res.status(200).json({ message: "Event deleted successfully" });
  } catch (error) {
    console.error("Error deleting event:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

module.exports = {
  getEvents,
  getEventsbyId,
  deleteEvent,
  createEvent,
  EventsthisWeek,
  getNightEvents,
  getTodayEvent,
  newEvent,
  
};
