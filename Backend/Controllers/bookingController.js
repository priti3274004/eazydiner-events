const {
  ticket,
  session,
  events,
  ticket_inventory,
  event_booking,
  Sequelize,
} = require("../models");
const { Op } = Sequelize;
const QRCode = require("qrcode");

const crypto = require("crypto");

const createBooking = async (req, res) => {
  const { name, contact, ticket_id, status, booking_date, no_of_persons } =
    req.body;
  try {
    const customer_id = req.user.id.toString();
    console.log(customer_id);
    const booking_Date = new Date();

    const inventory = await ticket_inventory.findOne({
      where: { ticket_id },
    });

    if (!ticket_id) {
      return res
        .status(404)
        .json({ error: "Please select tickets from here " });
    }

    if (!inventory) {
      return res.status(404).json({ error: "Ticket inventory not found" });
    }

    if (inventory.quantity < no_of_persons) {
      return res.status(400).json({ error: "Not enough tickets available" });
    }

    const hash = crypto.createHash("md5");
    hash.update(`${customer_id}$${name}${booking_Date}`);
    const hash_res = hash.digest("hex");
    const custom_booking_id = hash_res.substring(2, 10);

    QRCode.toDataURL(custom_booking_id, async (err, url) => {
      if (err) {
        console.error("Error generating QR code:", err);
        return res.status(500).json({ message: "Internal server error" });
      }

      const booking = await event_booking.create({
        customer_id,
        name,
        contact,
        ticket_id,
        status,
        booking_date,
        no_of_persons,
        custom_bookingId: custom_booking_id,
        qr_code: url,
      });

      inventory.quantity -= no_of_persons;
      await inventory.save();

      res
        .status(201)
        .json({ message: "Booking created successfully", booking });
    });
  } catch (error) {
    console.error("Error creating booking:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

const getBookingById = async (req, res) => {
  const { booking_id } = req.params;

  try {
    const booking = await event_booking.findOne({
      where: { id: booking_id },
      include: [
        {
          model: ticket,
          as: "ticket",
          include: [
            {
              model: session,
              as: "session",
              include: [
                {
                  model: events,
                  as: "events",
                },
              ],
            },
          ],
        },
      ],
    });

    if (!booking) {
      return res.status(404).json({ error: "Booking not found" });
    }
    res.json({ booking });
  } catch (error) {
    console.error("Error fetching booking:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};

const getBookings = async (req, res) => {
  try {
    const bookings = await event_booking.findAll({
      include: [
        {
          model: ticket,
          as: "ticket",
          include: [
            {
              model: ticket_inventory,
              as: "ticket_inventory",
            },
          ],
        },
      ],
    });

    res.status(200).json(bookings);
  } catch (error) {
    console.error("Error fetching bookings:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};
const getBookingByCustId = async (req, res) => {
  try {
    const { custId } = req.params;
    const bookings = await event_booking.findAll({
      where: { customer_id: custId },
      include: [
        {
          model: ticket,
          as: "ticket",
          include: [
            {
              model: session,
              as: "session",
              include: [
                {
                  model: events,
                  as: "events",
                },
              ],
            },
          ],
        },
      ],
      order: [["booking_date", "DESC"]],
    });

    if (bookings && bookings.length > 0) {
      res.status(200).json(bookings);
    } else {
      throw new Error("Booking not found for the customer");
    }
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

const cancelBooking = async (req, res) => {
  try {
    const bookingId = req.params.id;
    const booking = await event_booking.findByPk(bookingId, {
      include: {
        model: ticket,
        as: "ticket",
        include: {
          model: ticket_inventory,
          as: "ticket_inventory",
        },
      },
    });
    if (!booking) {
      return res.status(404).json({ error: "Booking Not found" });
    }
    const { no_of_persons } = booking;
    const ticketInventory = booking.ticket.ticket_inventory;
    if (booking.status !== "cancel") {
      ticketInventory.quantity += no_of_persons;
      await ticketInventory.save();
      await booking.update({ status: "cancel" });
      res.status(200).json({ message: "Booking cancelled successfully" });
    } else {
      res.status(400).json({ error: "Booking already cancelled" });
    }
  } catch (error) {
    console.error("Error cancelling booking:", error);
    res
      .status(500)
      .json({ error: "An error occurred while cancelling the booking" });
  }
};

module.exports = {
  createBooking,
  getBookings,
  getBookingByCustId,
  getBookingById,
  cancelBooking,
};

