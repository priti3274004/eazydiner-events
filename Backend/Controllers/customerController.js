const { Customer } = require("../models");
const jwt = require("jsonwebtoken");
const asyncHandler = require("express-async-handler");
const generateOTP = require("../Controllers/otpgenerator"); 
const { COOKIE_OPTIONS } = require("../constants/apiConstant");

const registerUser = asyncHandler(async (req, res) => {
  const { name, email, contact, otp } = req.body;

  try {
    if (!name || !/^[A-Za-z\s]+$/.test(name)) {
      return res.status(400).json({ message: "Please enter a valid name" });
    }
    if (!email) {
      return res.status(400).json({ message: "Please enter email" });
    }
    if (!contact || !/^[0-9]{10}$/.test(contact)) {
      return res.status(400).json({ message: "Please enter valid contact of 10 digits" });
    }

    const existingUser = await Customer.findOne({ where: { email } });
    

    if (existingUser && !otp) {
      return res.status(400).json({ message: "Email is already in use" });
    }

    const existingContact = await Customer.findOne({ where: { contact } });

    if (existingContact && !otp ) {
      return res.status(400).json({ message: "Mobile number is already in use" });
    }

    if (!existingUser) {
      const generatedOtp = generateOTP();
      const newUser = await Customer.create({
        name,
        email,
        contact,
        otp: generatedOtp,
        otpExpiresAt: new Date(Date.now() + 5 * 60 * 1000) 
      });

     
      return res.status(201).json({ message: "OTP sent to contact. Please verify to complete registration." });
    } else {
      if (existingUser.otpExpiresAt < new Date()) {
        return res.status(400).json({ message: "OTP has expired" });
      }

      if (existingUser.otp !== otp) {
        return res.status(400).json({ message: "Invalid OTP" });
      }

      
      await Customer.update(
        { otp: null, otpExpiresAt: null },
        { where: { id: existingUser.id } }
      );

      const token = jwt.sign(
        { id: existingUser.id, email: existingUser.email },
        process.env.JWT_SECRET_KEY,
        { expiresIn: "1h" }
      );

      return res
        .status(200)
        .cookie("AccessToken", token, COOKIE_OPTIONS)
        .json({ message: "User registered successfully", token });
    }
  } catch (error) {
    console.error("Error registering user:", error);
    res.status(500).json({ message: "Internal server error" });
  }
});

const loginUser = asyncHandler(async (req, res) => {
  const { contact, otp } = req.body;

  try {
    const user = await Customer.findOne({ where: { contact } });

    if (!user) {
      return res.status(400).json({ message: "User not found" });
    }

    if (!otp) {
      const generatedOtp = generateOTP();

      await Customer.update(
        { otp: generatedOtp, otpExpiresAt: new Date(Date.now() + 5 * 60 * 1000) },
        { where: { id: user.id } }
      );

      return res.status(200).json({ message: "OTP sent to contact. Please verify to login." });
    } else {
      if (user.otpExpiresAt < new Date()) {
        return res.status(400).json({ message: "OTP has expired" });
      }

      if (user.otp !== otp) {
        return res.status(400).json({ message: "Invalid OTP" });
      }

      // Clear OTP
      await Customer.update(
        { otp: null, otpExpiresAt: null },
        { where: { id: user.id } }
      );

      const token = jwt.sign(
        { id: user.id, email: user.email },
        process.env.JWT_SECRET_KEY,
        { expiresIn: "1h" }
      );

      return res
        .status(200)
        .cookie("AccessToken", token, COOKIE_OPTIONS)
        .json({
          message: "Login successful",
          token,
          user: {
            id: user.id,
            name: user.name,
            email: user.email,
            createdAt: user.createdAt,
          },
        });
    }
  } catch (error) {
    console.error("Error logging in:", error);
    res.status(500).json({ message: "Internal server error" });
  }
});

module.exports = { registerUser, loginUser };
